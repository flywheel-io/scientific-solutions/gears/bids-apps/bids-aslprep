"""Parser module to parse gear config.json."""

import logging
import os
from collections import defaultdict
from pathlib import Path
from typing import Dict, Tuple

from flywheel_bids.flywheel_bids_app_toolkit import BIDSAppContext
from flywheel_bids.flywheel_bids_app_toolkit.compression import unzip_archive_files
from flywheel_bids.flywheel_bids_app_toolkit.prep import load_recon_all_results
from flywheel_bids.flywheel_bids_app_toolkit.hpc_utils import reset_FS_subj_paths
from flywheel_bids.flywheel_bids_app_toolkit.utils.helpers import reconcile_existing_and_unzipped_files
from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns relevant
# inputs and options.
def parse_config(gear_context: GearToolkitContext) -> Tuple[bool, Dict]:
    """Search config for extra settings not used by BIDSAppContext.

    Args:
        gear_context (GearToolkitContext):

    Returns:
        debug (bool): Level of gear verbosity
        config_options (Dict): other config options explicitly called out
                in the manifest, not encapsulated by the bids_app_command
                field. The bids_app_command field is intended to handle the
                majority of the use cases to run the BIDS app.
    """

    debug = gear_context.config.get("debug")
    config_options = {}
    for key in gear_context.config.keys():
        # If there are extra kwargs that were highlighted in the Config UI
        # by putting them in the manifest specifically, add them to the list
        # to exclude (i.e., after "bids_app_command")
        if not key.startswith("gear-") and key not in ["debug", "bids_app_command", "archived_runs"]:
            config_options[key] = gear_context.config.get(key)

    return debug, config_options


def parse_input_files(gear_context: GearToolkitContext, app_context: BIDSAppContext):
    """
    Fetch the input files from the manifest and return the filepaths.

    Grab the manifest blob for inputs. Refine to only file inputs to be included in
    the BIDS App command. The method will create a key-value pair entry for an
    input_files dictionary. Best practice design will be to use the kwarg in the
    BIDS App command as the input file label (key).

    Create exclusion criteria for input files that are not to be included for the
    BIDS App command.
    """
    inputs = gear_context.manifest.get("inputs")
    input_files = defaultdict()
    # include_keys are files that are specific to the gear, i.e., added to the manifest
    # to fill a kwarg in the BIDS algorithm command. (e.g., "bids_filter_file")
    # NOTE: archived_runs, previous_runs are already unzipped when the BIDSAppContext
    # class is called at the beginning of the gear run. Don't include them here.
    include_keys = ["fs-subject-dir", "bids-filter-file", "use-plugin", "derivatives", "config-file"]

    # Field specific methods
    if "fs-subject-dir" in gear_context.manifest.get("inputs", None):
        # Reset the SUBJECT_DIR env var before trying to unzip and write
        # previous recon-all results
        reset_FS_subj_paths(gear_context)
        reconcile_existing_and_unzipped_files(os.environ["SUBJECTS_DIR"])
        input_files["fs-subject-dir"] = os.environ["SUBJECTS_DIR"]

    if inputs:
        remove_keys = []
        for i in inputs:
            if i in include_keys and inputs[i]["base"] == "file" and gear_context.get_input_path(i):
                input_files[i] = gear_context.get_input_path(i)
                if "zip" in Path(input_files[i]).suffix:
                    input_files[i] = unzip_archive_files(gear_context, i + "_unzip")
                # Take the key out so that it is not processed below
                remove_keys.append(i)

        # Process other input files; will not be passed back to be included
        # in the BIDS app command.
        for i in [ii for ii in inputs.keys() if ii not in remove_keys]:
            if inputs[i]["base"] == "file" and gear_context.get_input_path(i):
                input_path = gear_context.get_input_path(i)
                if "zip" in Path(input_path).suffix:
                    # Unzip into work, rather than inputs to be compatible with HPC
                    unzip_dir = unzip_archive_files(gear_context, i, Path(gear_context.work_dir / i))
                    # Add other clean up methods. Is the destination ID still in the
                    # folder path?
                    if i == "previous_results":
                        reconcile_existing_and_unzipped_files(app_context.analysis_output_dir, unzip_dir)
                    elif i == "freesurfer_recon_all_zip":
                        # Don't have to delete the key, b/c this input file path
                        # is not passed back to the be added to the BIDS App cmd
                        load_recon_all_results(unzip_dir, app_context.subject_label)

    return input_files
