# What to check/replace

manifest.json

- Do the inputs reflect the options in the gear?

config.json

- Do the inputs match available inputs? (Are there examples to borrow and
  anonymize from a platform run?)
- Does the `bids_app_command` reflect kwargs that are logical for the gear?
