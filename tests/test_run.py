from unittest.mock import patch

from flywheel_bids.flywheel_bids_app_toolkit.context import BIDSAppContext

from run import main


@patch("run.sys.exit")
@patch("run.package_output")
@patch("run.save_metadata")
@patch("run.run_bids_algo")
@patch("run.customize_bids_command")
@patch("run.generate_bids_command")
@patch("run.parse_input_files")
@patch("run.parse_config")
@patch("run.setup_bids_env")
@patch("run.check_and_link_dirs")
@patch("run.get_fw_details")
@patch("run.validate_setup")
@patch("run.BIDSAppContext")
def test_run(
    mock_BAC,
    mock_setup,
    get_fw_details,
    mock_check_links,
    setup_bids_env,
    parse_config,
    parse_input_files,
    generate_bids_command,
    customize_bids_command,
    run_bids_algo,
    save_metadata,
    package_output,
    sys_exit,
    extended_gear_context,
):
    # Set up mock objects and variables
    extended_gear_context.init_logging.return_value = None
    extended_gear_context.analysis_output_dir = "/path/to/output"
    extended_gear_context.bids_app_binary = "bids_app"
    mock_app_context = BIDSAppContext(extended_gear_context)
    mock_app_context.post_processing_only = False
    mock_app_context.gear_dry_run = False
    mock_BAC.return_value = mock_app_context

    # Set up mock functions
    get_fw_details.return_value = (
        "/path/to/destination",
        "gear_builder_info",
        "container",
    )
    setup_bids_env.return_value = []
    config_options = {"fake_config_opt": "howdy"}
    parse_config.return_value = (False, config_options)
    input_files = {"my_input_file": "my_input_filepath"}
    parse_input_files.return_value = input_files
    generate_bids_command.return_value = ["bids_command"]
    customize_bids_command.return_value = ["customized_bids_command"]
    run_bids_algo.return_value = 0

    # Call the main function
    main(extended_gear_context)

    # Assert the expected function calls and behaviors
    mock_BAC.assert_called_once_with(extended_gear_context)
    mock_setup.assert_called_once_with(extended_gear_context, mock_app_context)
    get_fw_details.assert_called_once_with(extended_gear_context)
    setup_bids_env.assert_called_once()
    parse_config.assert_called_once_with(extended_gear_context)
    generate_bids_command.assert_called_once_with(mock_app_context)
    customize_bids_command.assert_called_once_with(["bids_command"], config_options)
    run_bids_algo.assert_called_once_with(mock_app_context, ["customized_bids_command"])
    save_metadata.assert_called_once()
    package_output.assert_called_once()
