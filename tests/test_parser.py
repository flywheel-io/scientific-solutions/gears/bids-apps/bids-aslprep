"""Module to test parser.py"""

from collections import defaultdict
from unittest.mock import patch

import pytest

from fw_gear_bids_aslprep.parser import parse_config, parse_input_files


@pytest.mark.parametrize(
    "mock_config_dict, expected_debug, expected_config_options",
    [
        ({}, None, {}),
        ({"debug": "DEBUG"}, "DEBUG", {}),
        ({"debug": "ERROR", "my_opt": "is_boring"}, "ERROR", {"my_opt": "is_boring"}),
        (
            {"my_opt_2": "is_educated", "gear-special": "wd-40"},
            None,
            {"my_opt_2": "is_educated"},
        ),
    ],
)
def test_parse_config(mock_config_dict, expected_debug, expected_config_options, mock_context):
    mock_context.config.keys.side_effect = [mock_config_dict.keys()]
    mock_context.config.get.side_effect = lambda key: mock_config_dict.get(key)
    debug, config_options = parse_config(mock_context)
    assert debug == expected_debug
    assert config_options == expected_config_options


@patch("fw_gear_bids_aslprep.parser.load_recon_all_results")
@patch("fw_gear_bids_aslprep.parser.os.environ")
@patch("fw_gear_bids_aslprep.parser.reconcile_existing_and_unzipped_files")
@patch("fw_gear_bids_aslprep.parser.reset_FS_subj_paths")
@patch(
    "fw_gear_bids_aslprep.parser.unzip_archive_files",
    return_value="path/fs-subject-dir",
)
def test_parse_input_files(
    mock_unzip,
    mock_reset,
    mock_reconcile,
    mock_env,
    mock_recon_all,
    mock_context,
    extended_gear_context,
):
    mock_context.manifest = {
        "inputs": {
            "fs-subject-dir": {"base": "file"},
            "non_file1": {"base": "not_file"},
            "bids-filter-file": {"base": "file"},
            "something-else": {"base": "file"},
        }
    }
    mock_context.get_input_path = lambda x: f"path/{x}"
    mock_env.__getitem__.return_value = "path/fs-subject-dir"

    # Call the function with the mock GearToolkitContext object
    result = parse_input_files(mock_context, extended_gear_context)

    # Assert that the function returned the expected dictionary
    assert result == defaultdict(
        list,
        {
            "fs-subject-dir": "path/fs-subject-dir",
            "bids-filter-file": "path/bids-filter-file",
        },
    )
