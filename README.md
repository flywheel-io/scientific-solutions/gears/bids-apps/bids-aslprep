# BIDS ASLPrep (bids-aslprep)

# Update the BIDS algorithm version

1. Fork the repo.
2. [Update the DockerHub image](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-aslprep/-/blob/main/Dockerfile?ref_type=heads#L56) that the gear will use.
3. Update the [gear-builder line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-aslprep/-/blob/main/manifest.json?ref_type=heads#L48) in the manifest.
4. Update the [version line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-aslprep/-/blob/main/manifest.json?ref_type=heads#L159) in the manifest.
5. Run `poetry update` from the local commandline (where your cwd is the top-level of the gear). This command will update any dependencies for the Flywheel portion of the gear (not the BIDS algorithm itself!).
6. Run `fw-beta gear build` to update anything in the manifest.
7. Ideally, run `fw-beta gear upload` and complete a test run on your Flywheel instance.
8. Run `git checkout -b {my-update}`, `git commit -a -m "{My message about updating}" -n`, and `git push`.
9. Submit a merge request (MR) to the original gear repo for Flywheel staff to review for official inclusion in the exchange.

## Overview

[Usage](#usage)

[FAQ](#faq)

### Summary

ASLPrep preprocesses CBF data with tools from well-known software packages. For more information, check out the About section of the readthedocs, https://aslprep.readthedocs.io/en/latest/index.html#about

### Cite

Adebimpe, A., Bertolero, M., Dolui, S. et al. ASLPrep: a platform for processing of arterial spin labeled MRI and quantification of regional brain perfusion. Nat Methods 19, 683\u2013686 (2022). https://doi.org/10.1038/s41592-022-01458-7

### License

LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.

Gear: BSD-3-Clause, https://aslprep.readthedocs.io/en/latest/index.html#license-information
FSL: Non-commercial, see above
ANTS: Apache 2.0
AFNI: GNU GPL
FreeSurfer: GNU GPL + license file from MGH

### Classification

_Category:_ Analysis

_Gear Level:_

- [ ] Project
- [x] Subject
- [x] Session
- [ ] Acquisition
- [ ] Analysis

---

[[_TOC_]]

---

### Inputs

- archived_runs

  - OPTIONAL
  - **Type**: file
  - If there are other files that will allow the algorithm to run or pick up analysis
    from a certain point, then the archive can be supplied here. This zip will be automatically unzipped within the gear. NOTE: When the BIDSAppContext is created, if this field is present (as "archived_runs"), the toolkit will unzip the attached file into the BIDS directory and update paths appropriately. See BIDSAppContext.check_archived_inputs for more details.

- bids-filter-file

  - OPTIONAL
  - **Type**: file
  - Per the [kwargs for fmriprep](https://fmriprep.org/en/stable/usage.html#options-for-filtering-bids-queries), submit a JSON file describing custom BIDS files for PyBIDS.

- bidsignore

  - OPTIONAL
  - **Type**: file
  - Provide a .bidsignore file to pass to the bids-validator

- config-file

  - OPTIONAL
  - **Type**: file
  - Pre-generated configuration file. Any args in the bids_app_command
    box will overwrite configs in this file.

- derivatives

  - OPTIONAL
  - **Type**: file
  - If there are other files that will allow the algorithm to run or pick up analysis from a certain point, then the archive can be supplied here. This zip will be automatically unzipped within the gear. NOTE: When the BIDSAppContext is created, if this field is present (as "derivatives"), the toolkit will unzip the attached file into the BIDS directory and update paths appropriately. See BIDSAppContext.check_derivatives for more details.

- freesurfer_license_file

  - OPTIONAL
  - **Type**: file
  - Populate the license key here or on the project to be able to use FreeSurfer. BIDSAppToolkit finds and copies the license to the $FREESURFER_HOME location.

- fs-subject-dir

  - OPTIONAL
  - **Type**: file
  - In line with the [kwargs allowed by ASLPrep](https://aslprep.readthedocs.io/en/latest/usage.html#specific-options-for-freesurfer-preprocessing), upload existing FreeSurfer subject directories.

- use-plugin
  - OPTIONAL
  - **Type**: file
  - Config file for nipype

NOTE: bids-database-dir (path to a PyBIDS datase folder) is not currently available in this gear. Contact Flywheel for further support or submit an MR to the gear repo to include.

### Config

- bids_app_command
  - OPTIONAL
  - **Type**: free-text
  - The gear will run the defaults for the algorithm without anything in this box. If
    you wish to provide the command as you would on a CLI, input the exact command
    here. Flywheel will automatically update the BIDS_dir, output_dir, and
    analysis_level. (
    e.g., `bids_app bids_dir output participant --valid-arg1 --valid-arg2`)
  - For more help to build the command, please see [REPLACE:<BIDS App documentation>](https://aslprep.readthedocs.io/en/latest/usage.html).
  - Note: If you use a kwarg here, don't worry about putting a value in the box for
    the same kwarg below. Any kwarg that you see called out in the config UI and that
    is given a value will supersede the kwarg:value given as part of the
    bids_app_command.
- debug

  - **Type**: Boolean
  - **Default**: false
  - Verbosity of log messages; default results in INFO level, True will log DEBUG level

- gear-dry-run

  - **Type**: Boolean
  - **Default**: false (set to true for template)
  - Do everything related to Flywheel except actually execute BIDS App command.
  - Note: This is NOT the same as running the BIDS app command with `--dry-run`. gear-dry-run will not actually download the BIDS data, attempt to run the BIDS app command, or do any metadata/result updating.

- gear-post-processing-only
  - **Type**: Boolean
  - **Default**: false
  - If an archive file is available, one can pick up with a previously analyzed
    dataset. This option is useful if the metadata changed (or needs to change). It is also useful for development.

### Custom

- analysis-level
  - For all intents and purposes, the current analysis-level should remain
    "participant". The BIDSAppContext tries to determine if the gear was launched
    from the project level (and would update to "group"), but ASLPrep remains as "participant" for now. This field is used to populate the BIDS App
    command with the proper participant/group arg.
- bids-app-binary (str)
  - aslprep
- bids-app-data-types
  - The types of data to process. If an algorithm does not use a specific type, remove
    the corresponding name of the BIDS directory from this list to exclude that type of
    data from download during `export_bids`.

### Outputs

#### Files

bids_tree.html

- A list of all BIDS formatted files and folders available as input to the BIDS App

aslprep\_{destination_container}.zip

- Output files from the algorithm. See [the documentation for Outputs](https://aslprep.readthedocs.io/en/latest/outputs.html) for deeper description of files created by ASLPrep, including QA, derivatives, confound matricies, and other metrics.

aslprep_log.txt

- Log from the ASLPrep algorithm

job.log

- Details on the gear execution on Flywheel

#### Metadata

No metadta is updated by this gear.

### Pre-requisites

#### Prerequisite Gear Runs

BIDS-curate must run successfully in order to fill in entities in file.info.BIDS. These fields are required for any BIDS app gear to be able to download the proper data.

A list of gears, in the order they need to be run:

1. **re-label_container**
   -Level: Project

2. **bids-curate**
   - Level: Project

#### Prerequisite Metadata

A description of any metadata that is needed for the gear to run.
If possible, list as many specific metadata objects that are required:

1. BIDS
   - {container}.info.BIDS
   - Enables files to be written to disk in BIDS format

## Usage

Flywheel BIDS App gears allow the user to provide the commandline input that they would normally run for the BIDS app. The BIDS App Toolkit parses any arguments beyond the typical `fmriprep BIDS_dir output_dir analysis_level` when creating the BIDSAppContext object (see flywheel_bids.flywheel_bids_app_toolkit.context.parse_bids_app_commands).

Flywheel BIDS App gears allow the user to provide the commandline input that they would normally run for the BIDS app. The BIDS App Toolkit parses any arguments beyond the typical `fmriprep BIDS_dir output_dir analysis_level` when creating the BIDSAppContext object (see flywheel_bids.flywheel_bids_app_toolkit.context.parse_bids_app_commands).

### Setup:

Before running BIDS curation on your data, you must first prepare your data with the following steps:

1. Run the [file-metadata-importer](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-metadata-importer) gear on all the acquisitions in your dataset
   - This step extracts the DICOM header info and stores it as Flywheel Metadata.
1. Run the [file-classifier](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-classifier) gear on all the acquisitions in your dataset
   - This step sets [classification metadata](https://docs.flywheel.io/User_Guides/user_data_classification/).
1. Run the [DCM2NIIX: DICOM to NIfTI converter](https://gitlab.com/flywheel-io/scientific-solutions/gears/dcm2niix) gear on all the acquisitions in your dataset
   - This step generates the NIfTI files that fMRIPrep needs from the DICOMS. It also copies all flywheel metadata from the DICOM to the NIfTI file (In this case, all the DICOM header information we extracted in step 1)
1. Run the [curate-bids gear](https://gitlab.com/flywheel-io/flywheel-apps/curate-bids) on the project. More information about BIDS Curation on Flywheel can be found [here](https://docs.flywheel.io/User_Guides/user_bids_getting_started/) and running the BIDS curation gear is described [here](https://docs.flywheel.io/Developer_Guides/dev_bids_curation_3_curation_gear_v2/). If you need to rename sessions or subjects before curation, you may find the gear helpful: [relabel-container](https://gitlab.com/flywheel-io/scientific-solutions/gears/relabel-container).

1. Run fMRIPrep on a session, subject, or project.

Steps 1 through 3 can be automatically carried out as [gear rules](https://docs.flywheel.io/hc/en-us/articles/360008553133-Project-Gear-Rules).

These steps MUST be done in this order. NIfTI file headers have significantly fewer fields than the DICOM headers. File metadata will be written to .json sidecars when the files are exported in the BIDS format as expected by the fMRIPrep BIDS App which is run by this gear.

### Running:

To run the gear, [session](https://docs.flywheel.io/User_Guides/user_analysis_gears/) or [subject](https://docs.flywheel.io/User_Guides/user_run_an_analysis_gear_on_a_subject/). See the instructions for running the BIDS curation gear [at different levels here](https://docs.flywheel.io/Developer_Guides/dev_bids_curation_3_curation_gear_v2/#curating-a-session).

If you run this gear on an entire project, it will take a very long time and likely lead to a disk full error because it will sequentially step through each subject. Instead, you can launch multiple bids_aslprep jobs on subjects or sessions in parallel. More details about running gears using the SDK can be found in this [tutorial](https://docs.flywheel.io/User_Guides/user_how_to_run_gears_as_a_batch_run_gears/).

### Workflow

```mermaid
graph LR;
    A[default fMRIPrep command]:::input --> D[Parser];
    B[optional fMRIPrep command]:::input --> D;
    C[Configuration<br>options]:::input --> D;
    D:::parser --> E((fMRIPrep));
    E:::gear --> F[HTML reports]:::parser;

    classDef parser fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Select and supply configuration options
1. Gear parses and cleans options and configurations for `ASLPrep`
1. Gear uploads HTML reports

### Logging

Debug is the highest number of messages and will report some locations/options and all errors. If unchecked in the configuration tab, only INFO and higher priority level log messages will be reported.

## FAQ

[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]

<!-- markdownlint-disable-file -->
